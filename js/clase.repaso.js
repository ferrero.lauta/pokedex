$(document).ready(function () {
    $.ajax({
        method: "GET",
        url: "https://pokeapi.co/api/v2/pokemon?offset=0&limit=200",
    }).done(function (respuestaPokemons) {
        respuestaPokemons.results.forEach(pokemon => {
            let option = new Option(pokemon.name.toUpperCase(), pokemon.url);
            $('#combo-pokemon').append($(option));
        });
        $('#combo-pokemon').change(() => {
            verDatos($('#combo-pokemon').val());
        });
    });
});

function verDatos(pokemonSeleccionado) {
    $.ajax({
        method: "GET",
        url: pokemonSeleccionado
    }).done(function (datosPokemonSeleccionado) {
        // Cuando obtengo los datos de la api, se muestra el título y la tarjeta del pokemon
        $(".tarjeta-pokemon").show();

        // Obtengo la imagen del pokemon para mostrarlo. Accedo al attr del elemento/etiqueta/tag "img" en el html y le pongo el valor obtenido en
        // el objeto de respuesta
        $("#imagen-pokemon").attr("src", datosPokemonSeleccionado.sprites.front_default);

        // Muestro el nombre del pokemon obtenido desde la api, en la etiqueta con el id "nombre-pokemon"
        $("#nombre-pokemon").html(datosPokemonSeleccionado.name.toUpperCase());

        // Borro la lista de habilidades por si tiene datos de un pokemon anterior
        $("#lista-habilidades").empty();

        // Agrego un list item para la lista no ordenada (ul), por cada habilidad que tenga el pokemon obtenido
        datosPokemonSeleccionado.abilities.forEach((habilidad) => {
            $("#lista-habilidades").append($('<li class="list-group-item">').text(habilidad.ability.name));
        })

        // Borro la lista de tipos por si tiene datos de un pokemon anterior
        $("#lista-tipos").empty();

        // Agrego un list item para la lista no ordenada (ul), por cada tipo que tenga el pokemon obtenido
        datosPokemonSeleccionado.types.forEach((tipo) => {
            $("#lista-tipos").append($('<li class="list-group-item">').text(tipo.type.name));
        })
    })

}